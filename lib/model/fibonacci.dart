import 'dart:convert';

class Fibonacci {
  int index;
  int value;
  String symbol;

  Fibonacci({
    required this.index,
    required this.value,
    required this.symbol,
  });
}
