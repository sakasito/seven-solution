import 'package:app_test/model/fibonacci.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';

class FibonacciListPage extends StatefulWidget {
  const FibonacciListPage({super.key});

  @override
  State<FibonacciListPage> createState() => _FibonacciListPageState();
}

class _FibonacciListPageState extends State<FibonacciListPage> {
  List<Fibonacci> fibonacciList = [];
  List<Fibonacci> fibonacciListInSheet = [];
  List<Fibonacci> cirFiboList = [];
  List<Fibonacci> sqaFiboList = [];
  List<Fibonacci> crosFiboList = [];
  final ScrollController _controller = ScrollController();
  Fibonacci currentItem = Fibonacci(index: 0, symbol: '', value: 0);
  Fibonacci currentItemPoped = Fibonacci(index: 99999, symbol: '', value: 0);
  late bool mode = false;

  @override
  void initState() {
    super.initState();
    if (fibonacciList.isEmpty) {
      fibonacciList = generateFibonacciList(40);
    }
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

// cir sqa cros
  String symbolSelector(int index) {
    if (index == 2) {
      return 'sqa';
    } else if (index == 3) {
      return 'cros';
    }

    if (index % 4 == 3) {
      if (mode) {
        return 'sqa';
      } else {
        return 'cros';
      }
    }

    if (index % 4 == 0) {
      if (mode) {
        mode = false;
      } else if (!mode) {
        mode = true;
      }
      return 'cir';
    }

    if (mode) {
      return 'cros';
    } else {
      return 'sqa';
    }
  }

  List<Fibonacci> generateFibonacciList(int n) {
    List<Fibonacci> fibList = [];
    int a = 0, b = 1;
    Fibonacci first = Fibonacci(index: 0, symbol: 'cir', value: 0);
    Fibonacci sencond = Fibonacci(index: 1, symbol: 'sqa', value: 1);
    fibList.add(first);
    fibList.add(sencond);
    for (int i = 2; i < n; i++) {
      int next = a + b;
      Fibonacci fib =
          Fibonacci(index: i, symbol: symbolSelector(i), value: next);
      fibList.add(fib);
      a = b;
      b = next;
    }
    return fibList;
  }

  Icon renderIcon(String symbol) {
    switch (symbol) {
      case 'sqa':
        return const Icon(
          Icons.crop_square,
          color: Colors.black,
          size: 25.0,
        );
      case 'cir':
        return const Icon(
          Icons.circle,
          color: Colors.black,
          size: 25.0,
        );
      case 'cros':
        return const Icon(
          Icons.close,
          color: Colors.black,
          size: 25.0,
        );
      default:
    }
    // return white Icon
    return const Icon(
      Icons.crop_square,
      color: Colors.white,
      size: 15.0,
    );
  }

  void scrollToIndex(int index) {
    final position = index * 39.0;
    _controller.animateTo(
      position,
      duration: Duration(seconds: 1),
      curve: Curves.easeInOut,
    );
  }

  void inBottomSheetItemSelected(Fibonacci fibonacci) {
    setState(() {
      if (fibonacci.symbol == 'cir') {
        cirFiboList.remove(fibonacci);
      } else if (fibonacci.symbol == 'sqa') {
        sqaFiboList.remove(fibonacci);
      } else if (fibonacci.symbol == 'cros') {
        crosFiboList.remove(fibonacci);
      }
      fibonacciList.insert(0, fibonacci);
      fibonacciList.sort(((a, b) => a.index.compareTo(b.index)));
    });
    currentItemPoped = fibonacci;
    Navigator.pop(context);
    scrollToIndex(fibonacci.index);
  }

  void renderBottomSheet(BuildContext context, Fibonacci fibonacci) {
    showModalBottomSheet<void>(
      context: context,
      isScrollControlled: true,
      builder: (BuildContext context) {
        return SizedBox(
          height: MediaQuery.of(context).size.height * 0.5,
          child: SingleChildScrollView(
            child: Column(
              children: [
                ...(fibonacciListInSheet.map((e) {
                  return InkWell(
                    onTap: () => inBottomSheetItemSelected(e),
                    child: Container(
                      color: currentItem.index == e.index
                          ? Colors.green[700]
                          : Colors.white,
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(10, 12, 10, 12),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Wrap(
                              children: [
                                Text('index: ${e.index}, '),
                                Text('number: ${e.value}'),
                              ],
                            ),
                            renderIcon(e.symbol)
                          ],
                        ),
                      ),
                    ),
                  );
                })),
              ],
            ),
          ),
        );
      },
    );
  }

  void setItemInSymbolList(String symbol, Fibonacci fibonacci) {
    if (symbol == 'cir') {
      cirFiboList.insert(0, fibonacci);
      cirFiboList.sort(((a, b) => a.index.compareTo(b.index)));
      fibonacciListInSheet = cirFiboList;
    } else if (symbol == 'sqa') {
      sqaFiboList.insert(0, fibonacci);
      sqaFiboList.sort(((a, b) => a.index.compareTo(b.index)));
      fibonacciListInSheet = sqaFiboList;
    } else if (symbol == 'cros') {
      crosFiboList.insert(0, fibonacci);
      crosFiboList.sort(((a, b) => a.index.compareTo(b.index)));
      fibonacciListInSheet = crosFiboList;
    }
  }

  void onItemSelected(BuildContext context, Fibonacci fibonacci) {
    setState(() {
      currentItem = fibonacci;
      fibonacciList.remove(fibonacci);
      setItemInSymbolList(fibonacci.symbol, fibonacci);
    });
    renderBottomSheet(context, fibonacci);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: ListView.builder(
      controller: _controller,
      itemCount: fibonacciList.length,
      itemBuilder: (context, index) {
        return Container(
          color: fibonacciList[index].index == currentItemPoped.index
              ? Colors.red
              : Colors.white,
          child: InkWell(
            onTap: () => onItemSelected(context, fibonacciList[index]),
            child: Padding(
              padding: const EdgeInsets.fromLTRB(10, 12, 10, 12),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Wrap(
                    children: [
                      Text('index: ${fibonacciList[index].index}, '),
                      Text('number${fibonacciList[index].value}'),
                    ],
                  ),
                  renderIcon(fibonacciList[index].symbol)
                ],
              ),
            ),
          ),
        );
      },
    ));
  }
}
